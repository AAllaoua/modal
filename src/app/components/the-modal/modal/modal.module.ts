import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TheModalComponent } from '../the-modal.component';



@NgModule({

  declarations: [TheModalComponent],
  exports: [TheModalComponent],
  imports: [
    CommonModule,
    IonicModule
  ],

  entryComponents: [TheModalComponent]
})
export class ModalModule { }
