import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TheModalComponent } from './the-modal.component';

describe('TheModalComponent', () => {
  let component: TheModalComponent;
  let fixture: ComponentFixture<TheModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TheModalComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TheModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
