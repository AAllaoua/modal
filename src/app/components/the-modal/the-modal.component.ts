
import { ModalController } from '@ionic/angular';
import { Component, OnInit, Input } from '@angular/core';
import * as moment from 'moment';


@Component({
  selector: 'app-the-modal',
  templateUrl: './the-modal.component.html',
  styleUrls: ['./the-modal.component.scss'],
})
export class TheModalComponent implements OnInit {


  @Input() modalcontroller: ModalController;

  constructor() {

    console.log('modal constructor');
  }


  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalcontroller.dismiss({
      'dismissed': true
    });
  }

  ngOnInit() {

    let now = moment().format('LLLL');
      console.log(now);
   }


   getDifference()
   {
     console.log('getDifference');

     var date2 = new Date('2013-02-08');

    var endDate = moment(date2);
    var duration = moment.duration(moment().diff(endDate));

    
    console.log(endDate);
    console.log(duration);
    console.log(['il reste : ', duration.days(), 'jours'])


    //console.log(duration.hours);
    //console.log(duration.minutes);
    //console.log(duration.seconds);
    return duration;
  }

}
