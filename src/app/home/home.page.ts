import { TheModalComponent } from './../components/the-modal/the-modal.component';
import { Component } from '@angular/core';
import { ModalController } from '@ionic/angular';
import * as moment from 'moment';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(public modalcontroller: ModalController) {}

  private timer = setInterval (this.remainTime, 1000);


  async PresentModal()
  {
    console.log("PresentModal");    
      const modal = await this.modalcontroller.create({
        component: TheModalComponent,
        componentProps: {
          'modalcontroller': this.modalcontroller,
          }
      });

      
      return await modal.present();
  }


  remainTime()
  {
    console.log("Time is done");
    
  }







}
